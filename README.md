
# Bolsa de Valores 

Proyecto de prueba para miflink.com




## Documentación

### Diagrama Entidad Relación
![ERD](https://storage.googleapis.com/bawbam-blog/flink/Diagrama%20en%20blanco.png)

### Web App

* CRUD para empresas.

### API Rest


#### Obtener todas las empresas

```http
  GET http://35.202.127.36/api/v1/factories
```

#### Obtener detalle de una empresa

```http
  GET /http://35.202.127.36/api/v1/factories/{factory-id}
```


### Tech Stack

* Django 3.2.3
* Django Rest Framework
* Python 3.9
* Postgres 13.0
* Virtualenv
* Gitlab (Cliente de Git)

### Librerias

* pre-commit = https://github.com/pre-commit/pre-commit
* Coverage = https://github.com/nedbat/coveragepy
* Black = https://github.com/psf/black
* Pytest = https://docs.pytest.org/en/stable/
* Psycopg2-binary = https://www.psycopg.org/
* Flake8 = https://github.com/PyCQA/flake8
* Pudb = https://pypi.org/project/pudb/
* Python-decouple = https://pypi.org/project/python-decouple/


### Git Flow 

Flujo de trabajo [Git Flow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow)


### Configuración Pre-commit

#### Instalar el paquete

  ```bash  
  pip install pre-commit==2.12.0
  ```  

#### Agregar configuración(.pre-commit-config.yaml)

  ```bash
  repos:
  -   repo: https://github.com/ambv/black
      rev: 20.8b1
      hooks:
      - id: black
        language_version: python3.9.1
  -   repo: https://gitlab.com/pycqa/flake8
      rev: 3.9.0
      hooks:
      - id: flake8
  -   repo: https://github.com/Lucas-C/pre-commit-hooks-safety
      rev: v1.2.1
      hooks:
      -   id: python-safety-dependencies-check
  ```

#### Instalar pre-commit en nuestro repositorio

  ```bash
  pre-commit install
  ```

### Ejecutar localmente el proyecto

Crear entorno virtual
```bash
  virtualenv -p python3.9 .env
```

Clonar el proyecto

```bash
  git clone https://gitlab.com/IvanMSP/flink_valores.git
```

Instalar los requirements.txt

```bash
  pip install -r requirements/develop.txt
```

Agregar las variables de entorno al archivo .env



### Variables de Entorno

Para poder correr el proyecto necesitas agregar las siguientes variables a tu archivo .env

`NAMEDB`

`USERDB`

`PASSWORDDB`

`PORTDB`

`SECRET_KEY`

`DEBUG`

`SETTINGS_MODULE`



### Ejecutar Tests

```bash
pytest -s -v --cov=app apps/tests/ --cov-report=html
```
  
### Demos & Repositorios

* Web App Empresas: http://35.202.127.36/web/factories

* Repositorio: https://gitlab.com/IvanMSP/flink_valores.git

