# Thirdy-Party
from rest_framework import serializers

# Owner
from factory.models import Factory


class FactorySerializer(serializers.ModelSerializer):
    identificationNumber = serializers.CharField(source="identification_number")
    valuesMarket = serializers.CharField(source="values_market")

    class Meta:
        model = Factory
        fields = [
            "pk",
            "identificationNumber",
            "name",
            "description",
            "symbol",
            "valuesMarket",
        ]
