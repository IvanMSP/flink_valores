# Core Django
from django.urls import path

# Owner
from .views import FactoryList, FactoryDetail


urlspatterns = [
    path("factories", FactoryList.as_view(), name="factories"),
    path("factories/<int:pk>", FactoryDetail.as_view(), name="factory-detail"),
]
