# Django Core
from django.core.exceptions import ValidationError

# Thirdy Party
from rest_framework import status
from rest_framework.generics import ListAPIView

# Owner
from factory.models import Factory
from .serializers import FactorySerializer
from ..flink_framework.views import RetrieveView


class FactoryList(ListAPIView):
    queryset = Factory.objects.get_queryset().order_by("-created")
    serializer_class = FactorySerializer


class FactoryDetail(RetrieveView):
    serializer_class = FactorySerializer
    queryset = Factory.objects.all()
