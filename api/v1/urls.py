# Django Core
from django.urls import include, path

# Owner
from .factory.urls import urlspatterns as factory_urls

urlpatterns = [
    path("v1/", include(factory_urls)),
]
