# Thirdy Party
from import_export.admin import ImportExportModelAdmin

# Django Core
from django.contrib import admin
from django.db import models

# Owner
from .models import Factory, TickerNY


@admin.register(Factory)
class FactoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "symbol",
        "identification_number",
    ]


@admin.register(TickerNY)
class TickerNYAdmin(ImportExportModelAdmin):
    list_display = [
        "symbol",
    ]
