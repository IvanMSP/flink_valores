# Python
import random


def get_values_market():
    """
    Function for get values market random

    Used: Factory Model
    """
    values_list = [random.randrange(1, 100, 1) for i in range(50)]
    return values_list
