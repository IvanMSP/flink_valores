# Python
import uuid
import random

# Django Core
from django.core.exceptions import ImproperlyConfigured
from django.db import models

# Owner
from reusable.constants import REQUIRED, BLANK
from reusable.models import TimeStampModel
from .helpers import get_values_market


class TickerNY(TimeStampModel):
    """
    Modelo Ticker
    """

    symbol = models.CharField(verbose_name="Simbolo", max_length=5, **REQUIRED)
    description = models.CharField(
        verbose_name="Descripción", max_length=250, **REQUIRED
    )

    class Meta:
        verbose_name = "Ticker"
        verbose_name_plural = "Tickers"

    def __str__(self) -> str:
        return self.symbol


class Factory(TimeStampModel):
    """
    Modelo Factory
    """

    identification_number = models.UUIDField(
        default=uuid.uuid4, editable=False, **REQUIRED
    )
    name = models.CharField(verbose_name="Nombre", max_length=50, **REQUIRED)
    description = models.CharField(verbose_name="Descripción", max_length=100, **BLANK)
    symbol = models.CharField(verbose_name="Simbolo", max_length=5, **REQUIRED)
    values_market = models.TextField(
        default=get_values_market(), verbose_name="Valores Mercado", **BLANK
    )

    class Meta:
        verbose_name = "Empresa"
        verbose_name_plural = "Empresas"

    def __str__(self) -> str:
        return self.name
