# Thirdy party
from import_export import resources

# Onwer
from .models import TickerNY


class TickerNYResource(resources.ModelResource):
    class Meta:
        model = TickerNY
        fields = (
            "symbol",
            "description",
        )
