# Django Core
from django.contrib import admin
from django.urls import path
from django.urls.conf import include

# WebApp Urls
from webapp.urls import urlpatterns as webapp_urls

# API Urls
from api.v1.urls import urlpatterns as api_urls


urlpatterns = [
    path("admin/", admin.site.urls),
    path("web/", include(webapp_urls)),
    path("api/", include(api_urls)),
]
