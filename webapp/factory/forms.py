# Django Core
from django import forms
from django.core.validators import MaxLengthValidator

# Owner
from factory.models import Factory


class FactoryForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control input",
                "placeholder": "Nombre de la empresa",
            },
        ),
        label="Nombre Empresa",
        required=True,
    )
    description = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "class": "form-control textarea",
                "placeholder": "descripción",
                "rows": 4,
            }
        ),
        label="Descripción",
        required=False,
    )
    symbol = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control input",
                "placeholder": "solo cinco caracteres",
            }
        ),
        label="Simbolo",
    )

    class Meta:
        model = Factory
        fields = ("name", "description", "symbol", "values_market")
