# Django Core
from django.urls import include, path

# Owner
from webapp.factory.views import (
    FactoriesListView,
    FactoryCreateView,
    FactoryUpdateView,
    DeleteFactory,
)


urlpatterns = [
    path("factories", FactoriesListView.as_view(), name="factory-list"),
    path("factories/<int:pk>", DeleteFactory, name="factory-delete"),
    path("factory", FactoryCreateView.as_view(), name="factory-create"),
    path("factory/<int:pk>", FactoryUpdateView.as_view(), name="factory-update"),
]
