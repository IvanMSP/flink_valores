# Django Core
from django.views import View
from django.views.generic import ListView
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib import messages

# Owner
from webapp.factory.forms import FactoryForm
from factory.models import Factory, TickerNY


class FactoriesListView(View):
    template_name = "factory/factory_list.html"

    def get(self, request):
        factories = Factory.objects.all().order_by("-created")
        context = dict(factories=factories, count=factories.count())
        return render(request, self.template_name, context)


class FactoryCreateView(View):
    template_name = "factory/factory-form.html"

    def get(self, request):
        form = FactoryForm()
        context = dict(form=form)
        return render(request, self.template_name, context)

    def post(self, request):
        form = FactoryForm(request.POST)
        try:
            if form.is_valid():
                symbol = form.cleaned_data["symbol"]
                TickerNY.objects.get(symbol=symbol)
                form.save()
                return redirect("factory-list")
            if form.errors:
                for field in form:
                    for error in field.errors:
                        messages.add_message(
                            request, 40, f"Campo: {field.name} - {error}"
                        )
            context = dict(form=form)
            return render(request, self.template_name, context)

        except TickerNY.DoesNotExist:
            messages.error(
                request,
                f"simbolo no registrado en la bolsa de valores de New York: {symbol}",
            )
            settings.LOGGER.error(
                f" ==> Simbolo no registrado en la bolsa de valores de New York: {symbol}"
            )
            return redirect("factory-list")


class FactoryUpdateView(View):
    template_name = "factory/factory-form.html"

    def get(self, request, pk):
        factory_instance = Factory.objects.get(pk=pk)
        form = FactoryForm(instance=factory_instance)
        context = dict(form=form)
        return render(request, self.template_name, context)

    def post(self, request, pk):
        factory_instance = Factory.objects.get(pk=pk)
        form = FactoryForm(request.POST, instance=factory_instance)
        try:
            if form.is_valid():
                symbol = form.cleaned_data["symbol"]
                TickerNY.objects.get(symbol=symbol)
                form.save()
                return redirect("factory-list")
            if form.errors:
                for field in form:
                    for error in field.errors:
                        messages.add_message(
                            request, 40, f"Campo: {field.name} - {error}"
                        )
            context = dict(form=form)
            return render(request, self.template_name, context)

        except TickerNY.DoesNotExist:
            messages.error(
                request,
                f"simbolo no registrado en la bolsa de valores de New York: {symbol}",
            )
            settings.LOGGER.error(
                f" ==> Simbolo no registrado en la bolsa de valores de New York: {symbol}"
            )
            return redirect("factory-list")


def DeleteFactory(request, pk):
    try:
        factory = Factory.objects.get(pk=pk)
        factory.delete()
        messages.success(request, "Empresa eliminada")
        return redirect("factory-list")
    except Factory.DoesNotExist:
        messages.error(request, f"Empresa no encontrada: {factory}")
        settings.LOGGER.error(f" ==> Empresa no encontrada: {factory}")
        return redirect("factory-list")
