# Django Core
from django.urls import include, path

# Owner
from webapp.factory.urls import urlpatterns as factory_urls


urlpatterns = [
    path("", include(factory_urls)),
]
